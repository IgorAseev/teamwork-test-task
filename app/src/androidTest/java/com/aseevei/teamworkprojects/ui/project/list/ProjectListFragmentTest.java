package com.aseevei.teamworkprojects.ui.project.list;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static com.google.common.truth.Truth.assertThat;
import static org.hamcrest.Matchers.not;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import androidx.navigation.fragment.NavHostFragment;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import com.aseevei.teamworkprojects.MainActivity;
import com.aseevei.teamworkprojects.R;
import com.aseevei.teamworkprojects.cache.PreferencesHelper;
import com.aseevei.testing.RestServiceUtils;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Tests for {@link ProjectListFragment}.
 */
@RunWith(AndroidJUnit4.class)
public class ProjectListFragmentTest {

    private static final String RESPONSE_FILE_NAME = "get_projects_response.json";
    private static final String EMPTY_RESPONSE_FILE_NAME = "get_empty_projects_response.json";
    private static final int PORT = 8080;
    private static final int CODE_ERROR = 502;
    private static final int CODE_OK = 200;

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class, true, false);
    private MockWebServer server;

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start(PORT);
    }

    @Test
    public void testProjectFragmentIsOpened() {
        setServerErrorResponse();
        launchActivity();
        assertThat(getNavigationFragment().getChildFragmentManager().getFragments().get(0))
                .isInstanceOf(ProjectListFragment.class);
    }

    @Test
    public void testProjectsIsLoaded() throws Exception {
        setServerOkResponse();
        launchActivity();
        onView(withId(R.id.button_retry)).check(matches(not(isDisplayed())));
        onView(withId(R.id.progress_loading)).check(matches(not(isDisplayed())));
        onView(withId(R.id.recycler)).check(matches(isDisplayed()));
    }

    @Test
    public void testNoProjectsIsLoaded() throws Exception {
        setServerOkEmptyResponse();
        launchActivity();
        onView(withId(R.id.button_retry)).check(matches(isDisplayed()));
        onView(withId(R.id.text_message)).check(matches(isDisplayed()));
        onView(withId(R.id.progress_loading)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testShowRetryButtonWhenError() {
        setServerErrorResponse();
        launchActivity();
        onView(withId(R.id.button_retry)).check(matches(isDisplayed()));
        onView(withId(R.id.progress_loading)).check(matches(not(isDisplayed())));
        onView(withId(R.id.recycler)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testLoadOnClickRetry() throws Exception {
        testShowRetryButtonWhenError();
        setServerOkResponse();
        onView(withId(R.id.button_retry)).perform(click());
        onView(withId(R.id.button_retry)).check(matches(not(isDisplayed())));
        onView(withId(R.id.progress_loading)).check(matches(not(isDisplayed())));
        onView(withId(R.id.recycler)).check(matches(isDisplayed()));
    }

    @Test
    public void testDataIsDisplayedAfterRotation() throws Exception {
        testProjectsIsLoaded();
        activityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        onView(withId(R.id.button_retry)).check(matches(not(isDisplayed())));
        onView(withId(R.id.progress_loading)).check(matches(not(isDisplayed())));
        onView(withId(R.id.recycler)).check(matches(isDisplayed()));
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

    private void setServerOkResponse() throws Exception {
        server.enqueue(new MockResponse()
                .setResponseCode(CODE_OK)
                .setBody(RestServiceUtils.getStringFromFile(InstrumentationRegistry.getInstrumentation().getContext(),
                        RESPONSE_FILE_NAME)));
    }

    private void setServerOkEmptyResponse() throws Exception {
        server.enqueue(new MockResponse()
                .setResponseCode(CODE_OK)
                .setBody(RestServiceUtils.getStringFromFile(InstrumentationRegistry.getInstrumentation().getContext(),
                        EMPTY_RESPONSE_FILE_NAME)));
    }

    private void setServerErrorResponse() {
        server.enqueue(new MockResponse().setResponseCode(CODE_ERROR));
    }

    private NavHostFragment getNavigationFragment() {
        return (NavHostFragment) activityRule.getActivity()
                .getSupportFragmentManager()
                .findFragmentById(R.id.container);
    }

    private void launchActivity() {
        activityRule.launchActivity(new Intent());
        new PreferencesHelper(activityRule.getActivity()).setLastCacheTime(0);
    }
}
