package com.aseevei.teamworkprojects.model

import java.io.Serializable

/**
 * Representation for a [ProjectUiModel] fetched from an external layer data source.
 */
class ProjectUiModel(val name: String, val description: String, val logo: String) : Serializable
