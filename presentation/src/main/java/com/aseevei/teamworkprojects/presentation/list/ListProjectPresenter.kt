package com.aseevei.teamworkprojects.presentation.list

import com.aseevei.teamworkprojects.domain.interactor.browse.GetProjects
import com.aseevei.teamworkprojects.domain.model.Project
import com.aseevei.teamworkprojects.presentation.mapper.ProjectMapper
import com.aseevei.teamworkprojects.presentation.model.ProjectView
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class ListProjectPresenter @Inject constructor(
    private val getProjectsUseCase: GetProjects,
    val projectMapper: ProjectMapper
) : ListProjectContract.Presenter {

    private var view: ListProjectContract.View? = null
    private var data: List<ProjectView>? = null
    private var isLoading = false

    override fun attachView(view: ListProjectContract.View) {
        this.view = view
        if (isLoading) {
            return
        }
        if (data == null) {
            getProjects()
        } else {
            onProjectsLoaded()
        }
    }

    override fun detachView() {
        this.view = null;
    }

    override fun stop() {
        getProjectsUseCase.dispose()
    }

    override fun getProjects() {
        isLoading = true
        getProjectsUseCase.execute(ProjectSubscriber())
    }

    inner class ProjectSubscriber : DisposableSingleObserver<List<Project>>() {

        override fun onSuccess(projects: List<Project>) {
            data = projects.map { projectMapper.mapToView(it) }
            isLoading = false
            onProjectsLoaded()
        }

        override fun onError(exception: Throwable) {
            isLoading = false
            view?.showErrorState()
        }
    }

    private fun onProjectsLoaded() {
        if (data?.isNotEmpty() == true) {
            view?.showProjects(data!!)
        } else {
            view?.showEmptyState()
        }
    }
}
