package com.aseevei.teamworkprojects.domain.interactor

import com.aseevei.teamworkprojects.domain.executor.PostExecutionThread
import com.aseevei.teamworkprojects.domain.executor.ThreadExecutor
import com.aseevei.teamworkprojects.domain.interactor.browse.GetProjects
import com.aseevei.teamworkprojects.domain.model.Project
import com.aseevei.teamworkprojects.domain.repository.ProjectRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class GetProjectTest {

    private lateinit var getProjects: GetProjects
    private lateinit var mockThreadExecutor: ThreadExecutor
    private lateinit var mockPostExecutionThread: PostExecutionThread
    private lateinit var mockProjectRepository: ProjectRepository

    @Before
    fun setUp() {
        mockThreadExecutor = mock()
        mockPostExecutionThread = mock()
        mockProjectRepository = mock()
        getProjects = GetProjects(mockProjectRepository, mockThreadExecutor, mockPostExecutionThread)
    }

    @Test
    fun buildUseCaseObservableCallsRepository() {
        getProjects.buildUseCaseObservable(null)
        verify(mockProjectRepository).getProjects()
    }

    @Test
    fun buildUseCaseObservableCompletes() {
        stubProjectRepositoryGetProjects(Single.just(mutableListOf()))
        val testObserver = getProjects.buildUseCaseObservable(null).test()
        testObserver.assertComplete()
    }

    @Test
    fun buildUseCaseObservableReturnsData() {
        val projects = mutableListOf(Project("Name", "Description", "Logo"))
        stubProjectRepositoryGetProjects(Single.just(projects))
        val testObserver = getProjects.buildUseCaseObservable(null).test()
        testObserver.assertValue(projects)
    }

    private fun stubProjectRepositoryGetProjects(single: Single<List<Project>>) {
        whenever(mockProjectRepository.getProjects()).thenReturn(single)
    }
}
