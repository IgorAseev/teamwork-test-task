package com.aseevei.teamworkprojects.cache.mapper;

import com.aseevei.teamworkprojects.cache.model.CachedProject;
import com.aseevei.teamworkprojects.data.model.ProjectEntity;
import javax.inject.Inject;

/**
 * Map a {@link CachedProject} instance to and from a {@link ProjectEntity} instance when data is moving between
 * this later and the Data layer
 */
public class ProjectEntityMapper implements EntityMapper<CachedProject, ProjectEntity> {

    @Inject
    public ProjectEntityMapper() {
    }

    @Override
    public ProjectEntity mapFromCached(CachedProject type) {
        return new ProjectEntity(type.getName(), type.getDescription(), type.getLogo());
    }

    @Override
    public CachedProject mapToCached(ProjectEntity type) {
        return new CachedProject(type.getName(), type.getDescription(), type.getLogo());
    }
}
