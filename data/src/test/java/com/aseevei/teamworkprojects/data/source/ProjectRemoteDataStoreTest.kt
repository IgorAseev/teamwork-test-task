package com.aseevei.teamworkprojects.data.source

import com.aseevei.teamworkprojects.data.repository.ProjectRemote
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectRemoteDataStoreTest {

    private lateinit var projectRemoteDataStore: ProjectRemoteDataStore
    private lateinit var projectRemote: ProjectRemote

    @Before
    fun setUp() {
        projectRemote = mock()
        projectRemoteDataStore = ProjectRemoteDataStore(projectRemote)
    }

    @Test(expected = UnsupportedOperationException::class)
    fun clearProjectsThrowsException() {
        projectRemoteDataStore.clearProjects().test()
    }

    @Test(expected = UnsupportedOperationException::class)
    fun saveProjectsThrowsException() {
        projectRemoteDataStore.saveProjects(mutableListOf()).test()
    }

    @Test
    fun getProjectsCompletes() {
        whenever(projectRemote.getProjects()).thenReturn(Single.just(mutableListOf()))
        val testObserver = projectRemote.getProjects().test()
        testObserver.assertComplete()
    }
}
