package com.aseevei.teamworkprojects.data.mapper

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.domain.model.Project
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectMapperTest {

    private lateinit var projectMapper: ProjectMapper

    @Before
    fun setUp() {
        projectMapper = ProjectMapper()
    }

    @Test
    fun mapFromEntityMapsData() {
        val projectEntity = ProjectEntity("Name", "Description", "Logo")
        val project = projectMapper.mapFromEntity(projectEntity)

        assertProjectDataEquality(projectEntity, project)
    }

    @Test
    fun mapToEntityMapsData() {
        val project = Project("Name", "Description", "Logo")
        val projectEntity = projectMapper.mapToEntity(project)

        assertProjectDataEquality(projectEntity, project)
    }

    private fun assertProjectDataEquality(projectEntity: ProjectEntity, project: Project) {
        assertThat(projectEntity.name).isEqualTo(project.name);
        assertThat(projectEntity.description).isEqualTo(project.description);
        assertThat(projectEntity.logo).isEqualTo(project.logo);
    }
}
