package com.aseevei.teamworkprojects.data

import com.aseevei.teamworkprojects.data.mapper.ProjectMapper
import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.data.source.ProjectDataStoreFactory
import com.aseevei.teamworkprojects.data.source.ProjectRemoteDataStore
import com.aseevei.teamworkprojects.domain.model.Project
import com.aseevei.teamworkprojects.domain.repository.ProjectRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Provides an implementation of the [ProjectRepository] interface for communicating to and from
 * data sources
 */
class ProjectDataRepository @Inject constructor(
    private val factory: ProjectDataStoreFactory,
    private val projectMapper: ProjectMapper
) : ProjectRepository {

    override fun clearProjects(): Completable {
        return factory.retrieveCacheDataStore().clearProjects()
    }

    override fun saveProjects(projects: List<Project>): Completable {
        return saveProjectEntities(projects.map { projectMapper.mapToEntity(it) })
    }

    private fun saveProjectEntities(projects: List<ProjectEntity>): Completable {
        return factory.retrieveCacheDataStore().saveProjects(projects)
    }

    override fun getProjects(): Single<List<Project>> {
        val dataStore = factory.retrieveDataStore()
        return dataStore.getProjects()
            .flatMap {
                if (dataStore is ProjectRemoteDataStore) {
                    saveProjectEntities(it).toSingle { it }
                } else {
                    Single.just(it)
                }
            }
            .map { list -> list.map { listItem -> projectMapper.mapFromEntity(listItem) } }
    }
}
