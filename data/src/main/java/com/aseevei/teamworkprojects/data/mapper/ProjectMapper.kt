package com.aseevei.teamworkprojects.data.mapper

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.domain.model.Project
import javax.inject.Inject

/**
 * Map a [ProjectEntity] to and from a [Project] instance when data is moving between
 * this later and the Domain layer
 */
open class ProjectMapper @Inject constructor(): Mapper<ProjectEntity, Project> {

    /**
     * Map a [ProjectEntity] instance to a [Project] instance
     */
    override fun mapFromEntity(type: ProjectEntity): Project {
        return Project(type.name, type.description, type.logo)
    }

    /**
     * Map a [Project] instance to a [ProjectEntity] instance
     */
    override fun mapToEntity(type: Project): ProjectEntity {
        return ProjectEntity(type.name, type.description, type.logo)
    }
}
