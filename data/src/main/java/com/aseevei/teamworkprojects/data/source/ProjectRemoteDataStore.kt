package com.aseevei.teamworkprojects.data.source

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.data.repository.ProjectDataStore
import com.aseevei.teamworkprojects.data.repository.ProjectRemote
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Implementation of the [ProjectDataStore] interface to provide a means of communicating
 * with the remote data source
 */
open class ProjectRemoteDataStore @Inject constructor(private val projectRemote: ProjectRemote) :
    ProjectDataStore {

    override fun clearProjects(): Completable {
        throw UnsupportedOperationException()
    }

    override fun saveProjects(projects: List<ProjectEntity>): Completable {
        throw UnsupportedOperationException()
    }

    /**
     * Retrieve a list of [ProjectEntity] instances from the API
     */
    override fun getProjects(): Single<List<ProjectEntity>> {
        return projectRemote.getProjects()
    }
}
